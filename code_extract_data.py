"""
Extract data
============

Brief:
    This is used to extract image data from video with custom fps
Detail:
    Use command line to parse input video, data path, and fps:
    --video, -v: assert error is raised when parsed video file name is invalid
    --data, -d: default name for output data is 'data' 
    --fps, -f: default fps is 0.5
"""

__author__ = ['Trung Le', 'Nhat Phong', 'Danh Doan']
__email__ = ['trung.lenguyen276@gmail.com', 'nhatphong9715@gmail.com', 'danhdoancv@gmail.com']
__date__ = "2021/03/29"
__status__ = "release"


# =============================================================================


import os
import argparse
import cv2


# =============================================================================


def get_args():
    parser = argparse.ArgumentParser(description='Comman line argument')
    parser.add_argument('--video', '-v', type=str, help='path to video folder')
    parser.add_argument('--fps', '-f', type=float, 
        required=False, default=0.5,
        help='extract frame')
    parser.add_argument('--data', '-d', type=str, 
        required=False, default='data',
        help='path to output images folder')
    args = parser.parse_args()

    return args

# =============================================================================


def extract_data(video_path, data_path, fps=0.5):
    assert os.path.exists(video_path), 'Invalid Video path'
    
    os.makedirs(data_path, exist_ok=True)

    # Read the video from specified path
    cam = cv2.VideoCapture(video_path)

    # try:
      
    # # creating a folder named data
    #     if not os.path.exists(data_path):
    #         os.makedirs(data_path)
  
    # # if not created then raise error
    # except OSError:
    #     print ('Error: Creating directory of data')
    
    
    # Find OpenCV version
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
    video_fps = cam.get(cv2.CAP_PROP_FPS)
        
    # frame
    current_frame = 0
    cnt = 0
    while True:
        
        # reading from frame
        ret,frame = cam.read()
        if not ret:
            break
        if current_frame % round(video_fps / fps) == 0:
            
            # if video is still left continue creating images
            file_name = str(cnt) + '.jpg'
            file_path = os.path.join(data_path, file_name)
            # file_path = f'{data_path}{os.sep}{file_name}

            cnt += 1
            print('creating frame: {}'.format(cnt))
            # writing the extracted images
            cv2.imwrite(file_path, frame)

            # increasing counter so that it will
            # show how many frames are created
        current_frame += 1
        
    
    # Release all space and windows once done
    cam.release()
    cv2.destroyAllWindows()


# =============================================================================


def main(args):
    extract_data(args.video, args.data, args.fps)


# =============================================================================


if __name__ == '__main__':
    print('[INFO]', 'Extract data')

    args = get_args()
    main(args)

    print('[INFO]', 'Process done')